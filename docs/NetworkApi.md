# NetworkApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**network**](NetworkApi.md#network) | **GET** /network | Get Network Information


<a name="network"></a>
# **network**
> NetworkInfo network()

Get Network Information

Get Network Information

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.NetworkApi;


NetworkApi apiInstance = new NetworkApi();
try {
    NetworkInfo result = apiInstance.network();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NetworkApi#network");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NetworkInfo**](NetworkInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


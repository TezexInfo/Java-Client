# OperationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOperation**](OperationApi.md#getOperation) | **GET** /operation/{operation_hash} | Get Operation


<a name="getOperation"></a>
# **getOperation**
> Operation getOperation(operationHash)

Get Operation

Get a specific Operation

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.OperationApi;


OperationApi apiInstance = new OperationApi();
String operationHash = "operationHash_example"; // String | The hash of the Operation to retrieve
try {
    Operation result = apiInstance.getOperation(operationHash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OperationApi#getOperation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **operationHash** | **String**| The hash of the Operation to retrieve |

### Return type

[**Operation**](Operation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


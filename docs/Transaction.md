
# Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  |  [optional]
**branch** | **String** |  |  [optional]
**source** | **String** |  |  [optional]
**publicKey** | **String** |  |  [optional]
**level** | **Integer** |  |  [optional]
**blockHash** | **String** |  |  [optional]
**counter** | **Integer** |  |  [optional]
**time** | [**DateTime**](DateTime.md) |  |  [optional]
**operations** | [**List&lt;TransactionOperation&gt;**](TransactionOperation.md) |  |  [optional]




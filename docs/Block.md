
# Block

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  |  [optional]
**netId** | **String** |  |  [optional]
**protocol** | **String** |  |  [optional]
**level** | **Integer** |  |  [optional]
**proto** | **String** |  |  [optional]
**successors** | [**List&lt;ChainStatus&gt;**](ChainStatus.md) |  |  [optional]
**predecessor** | **String** |  |  [optional]
**time** | [**DateTime**](DateTime.md) |  |  [optional]
**validationPass** | **String** |  |  [optional]
**data** | **String** |  |  [optional]
**chainStatus** | **String** |  |  [optional]
**operationsCount** | **Integer** |  |  [optional]
**operationsHash** | **String** |  |  [optional]
**baker** | **String** |  |  [optional]
**seedNonceHash** | **String** |  |  [optional]
**proofOfWorkNonce** | **String** |  |  [optional]
**signature** | **String** |  |  [optional]
**priority** | **Integer** |  |  [optional]
**operationCount** | **Integer** |  |  [optional]
**totalFee** | **String** |  |  [optional]
**operations** | [**BlockOperationsSorted**](BlockOperationsSorted.md) |  |  [optional]




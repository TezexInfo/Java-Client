
# Candlestick

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**o** | **String** |  |  [optional]
**h** | **String** |  |  [optional]
**l** | **String** |  |  [optional]
**c** | **String** |  |  [optional]
**vol** | **String** |  |  [optional]
**time** | [**DateTime**](DateTime.md) |  |  [optional]




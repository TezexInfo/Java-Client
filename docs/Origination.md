
# Origination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  |  [optional]
**branch** | **String** |  |  [optional]
**source** | **String** |  |  [optional]
**publicKey** | **String** |  |  [optional]
**fee** | **Integer** |  |  [optional]
**counter** | **Integer** |  |  [optional]
**operations** | [**List&lt;OriginationOperation&gt;**](OriginationOperation.md) |  |  [optional]
**level** | **Integer** |  |  [optional]
**blockHash** | **String** |  |  [optional]
**time** | [**DateTime**](DateTime.md) |  |  [optional]




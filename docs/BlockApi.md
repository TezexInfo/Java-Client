# BlockApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blocksAll**](BlockApi.md#blocksAll) | **GET** /blocks/all | Get All Blocks 
[**blocksByLevel**](BlockApi.md#blocksByLevel) | **GET** /blocks/{level} | Get All Blocks for a specific Level
[**blocksByLevelRange**](BlockApi.md#blocksByLevelRange) | **GET** /blocks/{startlevel}/{stoplevel} | Get All Blocks for a specific Level-Range
[**getBlock**](BlockApi.md#getBlock) | **GET** /block/{blockhash} | Get Block By Blockhash
[**getBlockDelegations**](BlockApi.md#getBlockDelegations) | **GET** /block/{blockhash}/operations/delegations | Get Delegations of a Block
[**getBlockEndorsements**](BlockApi.md#getBlockEndorsements) | **GET** /block/{blockhash}/operations/endorsements | Get Endorsements of a Block
[**getBlockOperationsSorted**](BlockApi.md#getBlockOperationsSorted) | **GET** /block/{blockhash}/operations | Get operations of a block, sorted
[**getBlockOriginations**](BlockApi.md#getBlockOriginations) | **GET** /block/{blockhash}/operations/originations | Get Originations of a Block
[**getBlockTransaction**](BlockApi.md#getBlockTransaction) | **GET** /block/{blockhash}/operations/transactions | Get Transactions of Block
[**recentBlocks**](BlockApi.md#recentBlocks) | **GET** /blocks/recent | returns the last 25 blocks


<a name="blocksAll"></a>
# **blocksAll**
> BlocksAll blocksAll(page, order, limit)

Get All Blocks 

Get all Blocks

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockApi;


BlockApi apiInstance = new BlockApi();
BigDecimal page = new BigDecimal(); // BigDecimal | Pagination, 200 tx per page max
String order = "order_example"; // String | ASC or DESC
Integer limit = 56; // Integer | Results per Page
try {
    BlocksAll result = apiInstance.blocksAll(page, order, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#blocksAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **BigDecimal**| Pagination, 200 tx per page max | [optional]
 **order** | **String**| ASC or DESC | [optional] [enum: ASC, DESC]
 **limit** | **Integer**| Results per Page | [optional]

### Return type

[**BlocksAll**](BlocksAll.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="blocksByLevel"></a>
# **blocksByLevel**
> List&lt;Block&gt; blocksByLevel(level)

Get All Blocks for a specific Level

Get all Blocks for a specific Level

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockApi;


BlockApi apiInstance = new BlockApi();
BigDecimal level = new BigDecimal(); // BigDecimal | The level of the Blocks to retrieve, includes abandoned
try {
    List<Block> result = apiInstance.blocksByLevel(level);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#blocksByLevel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **level** | **BigDecimal**| The level of the Blocks to retrieve, includes abandoned |

### Return type

[**List&lt;Block&gt;**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="blocksByLevelRange"></a>
# **blocksByLevelRange**
> BlockRange blocksByLevelRange(startlevel, stoplevel)

Get All Blocks for a specific Level-Range

Get all Blocks for a specific Level-Range

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockApi;


BlockApi apiInstance = new BlockApi();
BigDecimal startlevel = new BigDecimal(); // BigDecimal | lowest blocklevel to return
BigDecimal stoplevel = new BigDecimal(); // BigDecimal | highest blocklevel to return
try {
    BlockRange result = apiInstance.blocksByLevelRange(startlevel, stoplevel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#blocksByLevelRange");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **BigDecimal**| lowest blocklevel to return |
 **stoplevel** | **BigDecimal**| highest blocklevel to return |

### Return type

[**BlockRange**](BlockRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlock"></a>
# **getBlock**
> Block getBlock(blockhash)

Get Block By Blockhash

Get a block by its hash

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockApi;


BlockApi apiInstance = new BlockApi();
String blockhash = "blockhash_example"; // String | The hash of the Block to retrieve
try {
    Block result = apiInstance.getBlock(blockhash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#getBlock");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| The hash of the Block to retrieve |

### Return type

[**Block**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlockDelegations"></a>
# **getBlockDelegations**
> List&lt;Delegation&gt; getBlockDelegations(blockhash)

Get Delegations of a Block

Get all Delegations of a specific Block

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockApi;


BlockApi apiInstance = new BlockApi();
String blockhash = "blockhash_example"; // String | Blockhash
try {
    List<Delegation> result = apiInstance.getBlockDelegations(blockhash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#getBlockDelegations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash |

### Return type

[**List&lt;Delegation&gt;**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlockEndorsements"></a>
# **getBlockEndorsements**
> List&lt;Endorsement&gt; getBlockEndorsements(blockhash)

Get Endorsements of a Block

Get all Endorsements of a specific Block

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockApi;


BlockApi apiInstance = new BlockApi();
String blockhash = "blockhash_example"; // String | Blockhash
try {
    List<Endorsement> result = apiInstance.getBlockEndorsements(blockhash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#getBlockEndorsements");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash |

### Return type

[**List&lt;Endorsement&gt;**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlockOperationsSorted"></a>
# **getBlockOperationsSorted**
> BlockOperationsSorted getBlockOperationsSorted(blockhash)

Get operations of a block, sorted

Get the maximum Level we have seen, Blocks at this level may become abandoned Blocks later on

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockApi;


BlockApi apiInstance = new BlockApi();
String blockhash = "blockhash_example"; // String | The hash of the Block to retrieve
try {
    BlockOperationsSorted result = apiInstance.getBlockOperationsSorted(blockhash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#getBlockOperationsSorted");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| The hash of the Block to retrieve |

### Return type

[**BlockOperationsSorted**](BlockOperationsSorted.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlockOriginations"></a>
# **getBlockOriginations**
> List&lt;Origination&gt; getBlockOriginations(blockhash)

Get Originations of a Block

Get all Originations of a spcific Block

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockApi;


BlockApi apiInstance = new BlockApi();
String blockhash = "blockhash_example"; // String | Blockhash
try {
    List<Origination> result = apiInstance.getBlockOriginations(blockhash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#getBlockOriginations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash |

### Return type

[**List&lt;Origination&gt;**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlockTransaction"></a>
# **getBlockTransaction**
> List&lt;Transaction&gt; getBlockTransaction(blockhash)

Get Transactions of Block

Get all Transactions of a spcific Block

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockApi;


BlockApi apiInstance = new BlockApi();
String blockhash = "blockhash_example"; // String | Blockhash
try {
    List<Transaction> result = apiInstance.getBlockTransaction(blockhash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#getBlockTransaction");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash |

### Return type

[**List&lt;Transaction&gt;**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="recentBlocks"></a>
# **recentBlocks**
> List&lt;Block&gt; recentBlocks()

returns the last 25 blocks

Get all Blocks for a specific Level

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockApi;


BlockApi apiInstance = new BlockApi();
try {
    List<Block> result = apiInstance.recentBlocks();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#recentBlocks");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Block&gt;**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


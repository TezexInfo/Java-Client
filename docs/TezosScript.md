
# TezosScript

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_int** | **String** |  |  [optional]
**string** | **String** |  |  [optional]
**prim** | **String** |  |  [optional]
**args** | [**List&lt;TezosScript&gt;**](TezosScript.md) |  |  [optional]




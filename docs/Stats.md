
# Stats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statGroup** | **String** |  |  [optional]
**stat** | **String** |  |  [optional]
**value** | **String** |  |  [optional]
**start** | [**DateTime**](DateTime.md) |  |  [optional]
**end** | [**DateTime**](DateTime.md) |  |  [optional]




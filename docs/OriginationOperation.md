
# OriginationOperation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **String** |  |  [optional]
**managerPubkey** | **String** |  |  [optional]
**balance** | **Integer** |  |  [optional]
**spendable** | **Boolean** |  |  [optional]
**delegateable** | **Boolean** |  |  [optional]
**delegate** | **String** |  |  [optional]
**script** | [**TezosScript**](TezosScript.md) |  |  [optional]
**storage** | [**TezosScript**](TezosScript.md) |  |  [optional]




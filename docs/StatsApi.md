# StatsApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getStatistics**](StatsApi.md#getStatistics) | **GET** /stats/{group}/{stat}/{period} | Get Statistics
[**getStatsOverview**](StatsApi.md#getStatsOverview) | **GET** /stats/overview | Returns some basic Info


<a name="getStatistics"></a>
# **getStatistics**
> List&lt;Stats&gt; getStatistics(group, stat, period, startTime, endTime)

Get Statistics

Get Statistics

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.StatsApi;


StatsApi apiInstance = new StatsApi();
String group = "group_example"; // String | Block, Transaction, etc
String stat = "stat_example"; // String | 
String period = "period_example"; // String | 
DateTime startTime = new DateTime(); // DateTime | 
DateTime endTime = new DateTime(); // DateTime | 
try {
    List<Stats> result = apiInstance.getStatistics(group, stat, period, startTime, endTime);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatsApi#getStatistics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **String**| Block, Transaction, etc |
 **stat** | **String**|  |
 **period** | **String**|  |
 **startTime** | **DateTime**|  | [optional]
 **endTime** | **DateTime**|  | [optional]

### Return type

[**List&lt;Stats&gt;**](Stats.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getStatsOverview"></a>
# **getStatsOverview**
> StatsOverview getStatsOverview()

Returns some basic Info

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.StatsApi;


StatsApi apiInstance = new StatsApi();
try {
    StatsOverview result = apiInstance.getStatsOverview();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatsApi#getStatsOverview");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatsOverview**](StatsOverview.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


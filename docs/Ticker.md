
# Ticker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BTC** | **String** |  |  [optional]
**CNY** | **String** |  |  [optional]
**USD** | **String** |  |  [optional]
**EUR** | **String** |  |  [optional]




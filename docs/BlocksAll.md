
# BlocksAll

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxBlockLevel** | **Integer** |  |  [optional]
**blocks** | [**List&lt;Block&gt;**](Block.md) |  |  [optional]
**totalResults** | **Integer** |  |  [optional]
**currentPage** | **Integer** |  |  [optional]




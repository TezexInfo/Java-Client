
# BlockOperationsSorted

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactions** | [**List&lt;Transaction&gt;**](Transaction.md) |  |  [optional]
**originations** | [**List&lt;Origination&gt;**](Origination.md) |  |  [optional]
**delegations** | [**List&lt;Delegation&gt;**](Delegation.md) |  |  [optional]
**endorsements** | [**List&lt;Endorsement&gt;**](Endorsement.md) |  |  [optional]




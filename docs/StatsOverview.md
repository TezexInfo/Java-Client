
# StatsOverview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**priceUsd** | **String** |  |  [optional]
**priceBtc** | **String** |  |  [optional]
**blockTime** | **Integer** | Blocktime in seconds |  [optional]
**priority** | [**BigDecimal**](BigDecimal.md) |  |  [optional]




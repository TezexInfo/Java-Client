
# NetworkInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxLevel** | **Integer** |  |  [optional]
**blocktime** | **String** |  |  [optional]
**transactions24h** | **Integer** |  |  [optional]
**oeprations24h** | **String** |  |  [optional]





# ChainStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  |  [optional]
**chainStatus** | **String** |  |  [optional]
**level** | **Integer** |  |  [optional]




# AccountApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAccount**](AccountApi.md#getAccount) | **GET** /account/{account} | Get Account
[**getAccountBalance**](AccountApi.md#getAccountBalance) | **GET** /account/{account}/balance | Get Account Balance
[**getAccountLastSeen**](AccountApi.md#getAccountLastSeen) | **GET** /account/{account}/last_seen | Get last active date
[**getAccountOperationCount**](AccountApi.md#getAccountOperationCount) | **GET** /account/{account}/operations_count | Get operation count of Account
[**getAccountTransactionCount**](AccountApi.md#getAccountTransactionCount) | **GET** /account/{account}/transaction_count | Get transaction count of Account
[**getDelegationsForAccount**](AccountApi.md#getDelegationsForAccount) | **GET** /account/{account}/delegations | Get Delegations of this account
[**getDelegationsToAccount**](AccountApi.md#getDelegationsToAccount) | **GET** /account/{account}/delegated | Get Delegations to this account
[**getEndorsementsForAccount**](AccountApi.md#getEndorsementsForAccount) | **GET** /account/{account}/endorsements | Get Endorsements this Account has made
[**getTransactionForAccountIncoming**](AccountApi.md#getTransactionForAccountIncoming) | **GET** /account/{account}/transactions/incoming | Get Transaction
[**getTransactionForAccountOutgoing**](AccountApi.md#getTransactionForAccountOutgoing) | **GET** /account/{account}/transactions/outgoing | Get Transaction


<a name="getAccount"></a>
# **getAccount**
> Account getAccount(account)

Get Account

Get Acccount

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.AccountApi;


AccountApi apiInstance = new AccountApi();
String account = "account_example"; // String | The account
try {
    Account result = apiInstance.getAccount(account);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getAccount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account |

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getAccountBalance"></a>
# **getAccountBalance**
> String getAccountBalance(account)

Get Account Balance

Get Balance

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.AccountApi;


AccountApi apiInstance = new AccountApi();
String account = "account_example"; // String | The account
try {
    String result = apiInstance.getAccountBalance(account);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getAccountBalance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getAccountLastSeen"></a>
# **getAccountLastSeen**
> DateTime getAccountLastSeen(account)

Get last active date

Get LastSeen Date

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.AccountApi;


AccountApi apiInstance = new AccountApi();
String account = "account_example"; // String | The account
try {
    DateTime result = apiInstance.getAccountLastSeen(account);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getAccountLastSeen");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account |

### Return type

[**DateTime**](DateTime.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getAccountOperationCount"></a>
# **getAccountOperationCount**
> Integer getAccountOperationCount(account)

Get operation count of Account

Get Operation Count

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.AccountApi;


AccountApi apiInstance = new AccountApi();
String account = "account_example"; // String | The account
try {
    Integer result = apiInstance.getAccountOperationCount(account);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getAccountOperationCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getAccountTransactionCount"></a>
# **getAccountTransactionCount**
> Integer getAccountTransactionCount(account)

Get transaction count of Account

Get Transaction Count

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.AccountApi;


AccountApi apiInstance = new AccountApi();
String account = "account_example"; // String | The account
try {
    Integer result = apiInstance.getAccountTransactionCount(account);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getAccountTransactionCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getDelegationsForAccount"></a>
# **getDelegationsForAccount**
> List&lt;Delegation&gt; getDelegationsForAccount(account, before)

Get Delegations of this account

Get Delegations this Account has made

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.AccountApi;


AccountApi apiInstance = new AccountApi();
String account = "account_example"; // String | The account for which to retrieve Delegations
Integer before = 56; // Integer | Only Return Delegations before this blocklevel
try {
    List<Delegation> result = apiInstance.getDelegationsForAccount(account, before);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getDelegationsForAccount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve Delegations |
 **before** | **Integer**| Only Return Delegations before this blocklevel | [optional]

### Return type

[**List&lt;Delegation&gt;**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getDelegationsToAccount"></a>
# **getDelegationsToAccount**
> List&lt;Delegation&gt; getDelegationsToAccount(account, before)

Get Delegations to this account

Get that have been made to this Account

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.AccountApi;


AccountApi apiInstance = new AccountApi();
String account = "account_example"; // String | The account to which delegations have been made
Integer before = 56; // Integer | Only Return Delegations before this blocklevel
try {
    List<Delegation> result = apiInstance.getDelegationsToAccount(account, before);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getDelegationsToAccount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account to which delegations have been made |
 **before** | **Integer**| Only Return Delegations before this blocklevel | [optional]

### Return type

[**List&lt;Delegation&gt;**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getEndorsementsForAccount"></a>
# **getEndorsementsForAccount**
> List&lt;Endorsement&gt; getEndorsementsForAccount(account, before)

Get Endorsements this Account has made

Get Endorsements this Account has made

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.AccountApi;


AccountApi apiInstance = new AccountApi();
String account = "account_example"; // String | The account for which to retrieve Endorsements
Integer before = 56; // Integer | Only Return Delegations before this blocklevel
try {
    List<Endorsement> result = apiInstance.getEndorsementsForAccount(account, before);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getEndorsementsForAccount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve Endorsements |
 **before** | **Integer**| Only Return Delegations before this blocklevel | [optional]

### Return type

[**List&lt;Endorsement&gt;**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getTransactionForAccountIncoming"></a>
# **getTransactionForAccountIncoming**
> Transactions getTransactionForAccountIncoming(account, before)

Get Transaction

Get incoming Transactions for a specific Account

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.AccountApi;


AccountApi apiInstance = new AccountApi();
String account = "account_example"; // String | The account for which to retrieve incoming Transactions
Integer before = 56; // Integer | Only Return transactions before this blocklevel
try {
    Transactions result = apiInstance.getTransactionForAccountIncoming(account, before);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getTransactionForAccountIncoming");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve incoming Transactions |
 **before** | **Integer**| Only Return transactions before this blocklevel | [optional]

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getTransactionForAccountOutgoing"></a>
# **getTransactionForAccountOutgoing**
> Transactions getTransactionForAccountOutgoing(account, before)

Get Transaction

Get outgoing Transactions for a specific Account

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.AccountApi;


AccountApi apiInstance = new AccountApi();
String account = "account_example"; // String | The account for which to retrieve outgoing Transactions
Integer before = 56; // Integer | Only return transactions before this blocklevel
try {
    Transactions result = apiInstance.getTransactionForAccountOutgoing(account, before);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getTransactionForAccountOutgoing");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve outgoing Transactions |
 **before** | **Integer**| Only return transactions before this blocklevel | [optional]

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


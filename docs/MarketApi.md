# MarketApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**candlestick**](MarketApi.md#candlestick) | **GET** /price/{denominator}/{numerator}/{period} | Candlestick Data
[**ticker**](MarketApi.md#ticker) | **GET** /ticker/{numerator} | Get Ticker for a specific Currency


<a name="candlestick"></a>
# **candlestick**
> List&lt;Candlestick&gt; candlestick(denominator, numerator, period)

Candlestick Data

Returns CandleStick Prices

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.MarketApi;


MarketApi apiInstance = new MarketApi();
String denominator = "denominator_example"; // String | which currency
String numerator = "numerator_example"; // String | to which currency
String period = "period_example"; // String | Timeframe of one candle
try {
    List<Candlestick> result = apiInstance.candlestick(denominator, numerator, period);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MarketApi#candlestick");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **denominator** | **String**| which currency |
 **numerator** | **String**| to which currency |
 **period** | **String**| Timeframe of one candle |

### Return type

[**List&lt;Candlestick&gt;**](Candlestick.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="ticker"></a>
# **ticker**
> Ticker ticker(numerator)

Get Ticker for a specific Currency

Returns BTC, USD, EUR and CNY Prices

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.MarketApi;


MarketApi apiInstance = new MarketApi();
String numerator = "numerator_example"; // String | The level of the Blocks to retrieve
try {
    Ticker result = apiInstance.ticker(numerator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MarketApi#ticker");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numerator** | **String**| The level of the Blocks to retrieve |

### Return type

[**Ticker**](Ticker.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


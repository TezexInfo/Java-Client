
# TransactionOperation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **String** |  |  [optional]
**amount** | **Integer** |  |  [optional]
**destination** | **String** |  |  [optional]
**parameters** | [**TezosScript**](TezosScript.md) |  |  [optional]




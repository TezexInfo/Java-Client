# BlockchainApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blockheight**](BlockchainApi.md#blockheight) | **GET** /maxLevel | Get Max Blockheight


<a name="blockheight"></a>
# **blockheight**
> Level blockheight()

Get Max Blockheight

Get the maximum Level we have seen

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.BlockchainApi;


BlockchainApi apiInstance = new BlockchainApi();
try {
    Level result = apiInstance.blockheight();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockchainApi#blockheight");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Level**](Level.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


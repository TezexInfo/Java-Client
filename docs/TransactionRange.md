
# TransactionRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxBlockLevel** | **Integer** |  |  [optional]
**transactions** | [**List&lt;Transaction&gt;**](Transaction.md) |  |  [optional]
**totalResults** | **Integer** |  |  [optional]
**currentPage** | **Integer** |  |  [optional]




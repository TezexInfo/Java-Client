# TransactionApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTransaction**](TransactionApi.md#getTransaction) | **GET** /transaction/{transaction_hash} | Get Transaction
[**getTransactionsRecent**](TransactionApi.md#getTransactionsRecent) | **GET** /transactions/recent | Returns the last 50 Transactions
[**transactionsAll**](TransactionApi.md#transactionsAll) | **GET** /transactions/all | Get All Transactions
[**transactionsByLevelRange**](TransactionApi.md#transactionsByLevelRange) | **GET** /transactions/{startlevel}/{stoplevel} | Get All Transactions for a specific Level-Range


<a name="getTransaction"></a>
# **getTransaction**
> Transaction getTransaction(transactionHash)

Get Transaction

Get a specific Transaction

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.TransactionApi;


TransactionApi apiInstance = new TransactionApi();
String transactionHash = "transactionHash_example"; // String | The hash of the Transaction to retrieve
try {
    Transaction result = apiInstance.getTransaction(transactionHash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransactionApi#getTransaction");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transactionHash** | **String**| The hash of the Transaction to retrieve |

### Return type

[**Transaction**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getTransactionsRecent"></a>
# **getTransactionsRecent**
> List&lt;Transaction&gt; getTransactionsRecent()

Returns the last 50 Transactions

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.TransactionApi;


TransactionApi apiInstance = new TransactionApi();
try {
    List<Transaction> result = apiInstance.getTransactionsRecent();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransactionApi#getTransactionsRecent");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Transaction&gt;**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="transactionsAll"></a>
# **transactionsAll**
> TransactionRange transactionsAll(page, order, limit)

Get All Transactions

Get all Transactions

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.TransactionApi;


TransactionApi apiInstance = new TransactionApi();
BigDecimal page = new BigDecimal(); // BigDecimal | Pagination, 200 tx per page max
String order = "order_example"; // String | ASC or DESC
Integer limit = 56; // Integer | Results per Page
try {
    TransactionRange result = apiInstance.transactionsAll(page, order, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransactionApi#transactionsAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **BigDecimal**| Pagination, 200 tx per page max | [optional]
 **order** | **String**| ASC or DESC | [optional] [enum: ASC, DESC]
 **limit** | **Integer**| Results per Page | [optional]

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="transactionsByLevelRange"></a>
# **transactionsByLevelRange**
> TransactionRange transactionsByLevelRange(startlevel, stoplevel, page, order, limit)

Get All Transactions for a specific Level-Range

Get all Transactions for a specific Level-Range

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.TransactionApi;


TransactionApi apiInstance = new TransactionApi();
BigDecimal startlevel = new BigDecimal(); // BigDecimal | lowest blocklevel to return
BigDecimal stoplevel = new BigDecimal(); // BigDecimal | highest blocklevel to return
BigDecimal page = new BigDecimal(); // BigDecimal | Pagination, 200 tx per page max
String order = "order_example"; // String | ASC or DESC
Integer limit = 56; // Integer | Results per Page
try {
    TransactionRange result = apiInstance.transactionsByLevelRange(startlevel, stoplevel, page, order, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransactionApi#transactionsByLevelRange");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **BigDecimal**| lowest blocklevel to return |
 **stoplevel** | **BigDecimal**| highest blocklevel to return |
 **page** | **BigDecimal**| Pagination, 200 tx per page max | [optional]
 **order** | **String**| ASC or DESC | [optional] [enum: ASC, DESC]
 **limit** | **Integer**| Results per Page | [optional]

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


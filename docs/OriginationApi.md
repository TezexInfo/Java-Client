# OriginationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOrigination**](OriginationApi.md#getOrigination) | **GET** /origination/{origination_hash} | Get Origination


<a name="getOrigination"></a>
# **getOrigination**
> Origination getOrigination(originationHash)

Get Origination

Get a specific Origination

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.OriginationApi;


OriginationApi apiInstance = new OriginationApi();
String originationHash = "originationHash_example"; // String | The hash of the Origination to retrieve
try {
    Origination result = apiInstance.getOrigination(originationHash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OriginationApi#getOrigination");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **originationHash** | **String**| The hash of the Origination to retrieve |

### Return type

[**Origination**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


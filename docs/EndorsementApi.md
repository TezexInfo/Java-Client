# EndorsementApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEndorsement**](EndorsementApi.md#getEndorsement) | **GET** /endorsement/{endorsement_hash} | Get Endorsement
[**getEndorsementForBlock**](EndorsementApi.md#getEndorsementForBlock) | **GET** /endorsement/for/{block_hash} | Get Endorsement


<a name="getEndorsement"></a>
# **getEndorsement**
> Endorsement getEndorsement(endorsementHash)

Get Endorsement

Get a specific Endorsement

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.EndorsementApi;


EndorsementApi apiInstance = new EndorsementApi();
String endorsementHash = "endorsementHash_example"; // String | The hash of the Endorsement to retrieve
try {
    Endorsement result = apiInstance.getEndorsement(endorsementHash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EndorsementApi#getEndorsement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **endorsementHash** | **String**| The hash of the Endorsement to retrieve |

### Return type

[**Endorsement**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getEndorsementForBlock"></a>
# **getEndorsementForBlock**
> List&lt;Endorsement&gt; getEndorsementForBlock(blockHash)

Get Endorsement

Get a specific Endorsement

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.EndorsementApi;


EndorsementApi apiInstance = new EndorsementApi();
String blockHash = "blockHash_example"; // String | blockhash
try {
    List<Endorsement> result = apiInstance.getEndorsementForBlock(blockHash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EndorsementApi#getEndorsementForBlock");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockHash** | **String**| blockhash |

### Return type

[**List&lt;Endorsement&gt;**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


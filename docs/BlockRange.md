
# BlockRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxBlockLevel** | **Integer** |  |  [optional]
**blocks** | [**List&lt;Block&gt;**](Block.md) |  |  [optional]




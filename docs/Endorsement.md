
# Endorsement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  |  [optional]
**source** | **String** |  |  [optional]
**level** | **Integer** |  |  [optional]
**block** | **String** |  |  [optional]
**endorsedBlock** | **String** |  |  [optional]
**slot** | **Integer** |  |  [optional]
**time** | [**DateTime**](DateTime.md) |  |  [optional]




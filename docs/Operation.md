
# Operation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction** | [**Transaction**](Transaction.md) |  |  [optional]
**origination** | [**Origination**](Origination.md) |  |  [optional]
**delegation** | [**Delegation**](Delegation.md) |  |  [optional]




# DelegationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDelegation**](DelegationApi.md#getDelegation) | **GET** /delegation/{delegation_hash} | Get Delegation


<a name="getDelegation"></a>
# **getDelegation**
> Delegation getDelegation(delegationHash)

Get Delegation

Get a specific Delegation

### Example
```java
// Import classes:
//import info.tezex.ApiException;
//import info.tezex.javaclient.DelegationApi;


DelegationApi apiInstance = new DelegationApi();
String delegationHash = "delegationHash_example"; // String | The hash of the Origination to retrieve
try {
    Delegation result = apiInstance.getDelegation(delegationHash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DelegationApi#getDelegation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegationHash** | **String**| The hash of the Origination to retrieve |

### Return type

[**Delegation**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json


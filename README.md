# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import info.tezex.*;
import info.tezex.auth.*;
import info.tezex.model.*;
import info.tezex.javaclient.AccountApi;

import java.io.File;
import java.util.*;

public class AccountApiExample {

    public static void main(String[] args) {
        
        AccountApi apiInstance = new AccountApi();
        String account = "account_example"; // String | The account
        try {
            Account result = apiInstance.getAccount(account);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountApi#getAccount");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://betaapi.tezex.info/v2*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountApi* | [**getAccount**](docs/AccountApi.md#getAccount) | **GET** /account/{account} | Get Account
*AccountApi* | [**getAccountBalance**](docs/AccountApi.md#getAccountBalance) | **GET** /account/{account}/balance | Get Account Balance
*AccountApi* | [**getAccountLastSeen**](docs/AccountApi.md#getAccountLastSeen) | **GET** /account/{account}/last_seen | Get last active date
*AccountApi* | [**getAccountOperationCount**](docs/AccountApi.md#getAccountOperationCount) | **GET** /account/{account}/operations_count | Get operation count of Account
*AccountApi* | [**getAccountTransactionCount**](docs/AccountApi.md#getAccountTransactionCount) | **GET** /account/{account}/transaction_count | Get transaction count of Account
*AccountApi* | [**getDelegationsForAccount**](docs/AccountApi.md#getDelegationsForAccount) | **GET** /account/{account}/delegations | Get Delegations of this account
*AccountApi* | [**getDelegationsToAccount**](docs/AccountApi.md#getDelegationsToAccount) | **GET** /account/{account}/delegated | Get Delegations to this account
*AccountApi* | [**getEndorsementsForAccount**](docs/AccountApi.md#getEndorsementsForAccount) | **GET** /account/{account}/endorsements | Get Endorsements this Account has made
*AccountApi* | [**getTransactionForAccountIncoming**](docs/AccountApi.md#getTransactionForAccountIncoming) | **GET** /account/{account}/transactions/incoming | Get Transaction
*AccountApi* | [**getTransactionForAccountOutgoing**](docs/AccountApi.md#getTransactionForAccountOutgoing) | **GET** /account/{account}/transactions/outgoing | Get Transaction
*BlockApi* | [**blocksAll**](docs/BlockApi.md#blocksAll) | **GET** /blocks/all | Get All Blocks 
*BlockApi* | [**blocksByLevel**](docs/BlockApi.md#blocksByLevel) | **GET** /blocks/{level} | Get All Blocks for a specific Level
*BlockApi* | [**blocksByLevelRange**](docs/BlockApi.md#blocksByLevelRange) | **GET** /blocks/{startlevel}/{stoplevel} | Get All Blocks for a specific Level-Range
*BlockApi* | [**getBlock**](docs/BlockApi.md#getBlock) | **GET** /block/{blockhash} | Get Block By Blockhash
*BlockApi* | [**getBlockDelegations**](docs/BlockApi.md#getBlockDelegations) | **GET** /block/{blockhash}/operations/delegations | Get Delegations of a Block
*BlockApi* | [**getBlockEndorsements**](docs/BlockApi.md#getBlockEndorsements) | **GET** /block/{blockhash}/operations/endorsements | Get Endorsements of a Block
*BlockApi* | [**getBlockOperationsSorted**](docs/BlockApi.md#getBlockOperationsSorted) | **GET** /block/{blockhash}/operations | Get operations of a block, sorted
*BlockApi* | [**getBlockOriginations**](docs/BlockApi.md#getBlockOriginations) | **GET** /block/{blockhash}/operations/originations | Get Originations of a Block
*BlockApi* | [**getBlockTransaction**](docs/BlockApi.md#getBlockTransaction) | **GET** /block/{blockhash}/operations/transactions | Get Transactions of Block
*BlockApi* | [**recentBlocks**](docs/BlockApi.md#recentBlocks) | **GET** /blocks/recent | returns the last 25 blocks
*BlockchainApi* | [**blockheight**](docs/BlockchainApi.md#blockheight) | **GET** /maxLevel | Get Max Blockheight
*DelegationApi* | [**getDelegation**](docs/DelegationApi.md#getDelegation) | **GET** /delegation/{delegation_hash} | Get Delegation
*EndorsementApi* | [**getEndorsement**](docs/EndorsementApi.md#getEndorsement) | **GET** /endorsement/{endorsement_hash} | Get Endorsement
*EndorsementApi* | [**getEndorsementForBlock**](docs/EndorsementApi.md#getEndorsementForBlock) | **GET** /endorsement/for/{block_hash} | Get Endorsement
*MarketApi* | [**candlestick**](docs/MarketApi.md#candlestick) | **GET** /price/{denominator}/{numerator}/{period} | Candlestick Data
*MarketApi* | [**ticker**](docs/MarketApi.md#ticker) | **GET** /ticker/{numerator} | Get Ticker for a specific Currency
*NetworkApi* | [**network**](docs/NetworkApi.md#network) | **GET** /network | Get Network Information
*OperationApi* | [**getOperation**](docs/OperationApi.md#getOperation) | **GET** /operation/{operation_hash} | Get Operation
*OriginationApi* | [**getOrigination**](docs/OriginationApi.md#getOrigination) | **GET** /origination/{origination_hash} | Get Origination
*StatsApi* | [**getStatistics**](docs/StatsApi.md#getStatistics) | **GET** /stats/{group}/{stat}/{period} | Get Statistics
*StatsApi* | [**getStatsOverview**](docs/StatsApi.md#getStatsOverview) | **GET** /stats/overview | Returns some basic Info
*TransactionApi* | [**getTransaction**](docs/TransactionApi.md#getTransaction) | **GET** /transaction/{transaction_hash} | Get Transaction
*TransactionApi* | [**getTransactionsRecent**](docs/TransactionApi.md#getTransactionsRecent) | **GET** /transactions/recent | Returns the last 50 Transactions
*TransactionApi* | [**transactionsAll**](docs/TransactionApi.md#transactionsAll) | **GET** /transactions/all | Get All Transactions
*TransactionApi* | [**transactionsByLevelRange**](docs/TransactionApi.md#transactionsByLevelRange) | **GET** /transactions/{startlevel}/{stoplevel} | Get All Transactions for a specific Level-Range


## Documentation for Models

 - [Account](docs/Account.md)
 - [Block](docs/Block.md)
 - [BlockOperationsSorted](docs/BlockOperationsSorted.md)
 - [BlockRange](docs/BlockRange.md)
 - [BlocksAll](docs/BlocksAll.md)
 - [Candlestick](docs/Candlestick.md)
 - [ChainStatus](docs/ChainStatus.md)
 - [Delegation](docs/Delegation.md)
 - [Endorsement](docs/Endorsement.md)
 - [Level](docs/Level.md)
 - [NetworkInfo](docs/NetworkInfo.md)
 - [Operation](docs/Operation.md)
 - [Origination](docs/Origination.md)
 - [OriginationOperation](docs/OriginationOperation.md)
 - [Stats](docs/Stats.md)
 - [StatsOverview](docs/StatsOverview.md)
 - [TezosScript](docs/TezosScript.md)
 - [Ticker](docs/Ticker.md)
 - [Transaction](docs/Transaction.md)
 - [TransactionOperation](docs/TransactionOperation.md)
 - [TransactionRange](docs/TransactionRange.md)
 - [Transactions](docs/Transactions.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issue.

## Author

office@bitfly.at


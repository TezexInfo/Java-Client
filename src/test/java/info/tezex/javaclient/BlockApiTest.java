/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package info.tezex.javaclient;

import info.tezex.ApiException;
import io.swagger.client.model.BlocksAll;
import java.math.BigDecimal;
import io.swagger.client.model.Block;
import io.swagger.client.model.BlockRange;
import io.swagger.client.model.Delegation;
import io.swagger.client.model.Endorsement;
import io.swagger.client.model.BlockOperationsSorted;
import io.swagger.client.model.Origination;
import io.swagger.client.model.Transaction;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for BlockApi
 */
public class BlockApiTest {

    private final BlockApi api = new BlockApi();

    
    /**
     * Get All Blocks 
     *
     * Get all Blocks
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void blocksAllTest() throws ApiException {
        BigDecimal page = null;
        String order = null;
        Integer limit = null;
        // BlocksAll response = api.blocksAll(page, order, limit);

        // TODO: test validations
    }
    
    /**
     * Get All Blocks for a specific Level
     *
     * Get all Blocks for a specific Level
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void blocksByLevelTest() throws ApiException {
        BigDecimal level = null;
        // List<Block> response = api.blocksByLevel(level);

        // TODO: test validations
    }
    
    /**
     * Get All Blocks for a specific Level-Range
     *
     * Get all Blocks for a specific Level-Range
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void blocksByLevelRangeTest() throws ApiException {
        BigDecimal startlevel = null;
        BigDecimal stoplevel = null;
        // BlockRange response = api.blocksByLevelRange(startlevel, stoplevel);

        // TODO: test validations
    }
    
    /**
     * Get Block By Blockhash
     *
     * Get a block by its hash
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getBlockTest() throws ApiException {
        String blockhash = null;
        // Block response = api.getBlock(blockhash);

        // TODO: test validations
    }
    
    /**
     * Get Delegations of a Block
     *
     * Get all Delegations of a specific Block
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getBlockDelegationsTest() throws ApiException {
        String blockhash = null;
        // List<Delegation> response = api.getBlockDelegations(blockhash);

        // TODO: test validations
    }
    
    /**
     * Get Endorsements of a Block
     *
     * Get all Endorsements of a specific Block
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getBlockEndorsementsTest() throws ApiException {
        String blockhash = null;
        // List<Endorsement> response = api.getBlockEndorsements(blockhash);

        // TODO: test validations
    }
    
    /**
     * Get operations of a block, sorted
     *
     * Get the maximum Level we have seen, Blocks at this level may become abandoned Blocks later on
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getBlockOperationsSortedTest() throws ApiException {
        String blockhash = null;
        // BlockOperationsSorted response = api.getBlockOperationsSorted(blockhash);

        // TODO: test validations
    }
    
    /**
     * Get Originations of a Block
     *
     * Get all Originations of a spcific Block
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getBlockOriginationsTest() throws ApiException {
        String blockhash = null;
        // List<Origination> response = api.getBlockOriginations(blockhash);

        // TODO: test validations
    }
    
    /**
     * Get Transactions of Block
     *
     * Get all Transactions of a spcific Block
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getBlockTransactionTest() throws ApiException {
        String blockhash = null;
        // List<Transaction> response = api.getBlockTransaction(blockhash);

        // TODO: test validations
    }
    
    /**
     * returns the last 25 blocks
     *
     * Get all Blocks for a specific Level
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void recentBlocksTest() throws ApiException {
        // List<Block> response = api.recentBlocks();

        // TODO: test validations
    }
    
}

/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.TezosScript;
import java.util.ArrayList;
import java.util.List;


/**
 * TezosScript
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-06T13:45:21.847+01:00")
public class TezosScript   {
  @SerializedName("int")
  private String _int = null;

  @SerializedName("string")
  private String string = null;

  @SerializedName("prim")
  private String prim = null;

  @SerializedName("args")
  private List<TezosScript> args = new ArrayList<TezosScript>();

  public TezosScript _int(String _int) {
    this._int = _int;
    return this;
  }

   /**
   * Get _int
   * @return _int
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getInt() {
    return _int;
  }

  public void setInt(String _int) {
    this._int = _int;
  }

  public TezosScript string(String string) {
    this.string = string;
    return this;
  }

   /**
   * Get string
   * @return string
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getString() {
    return string;
  }

  public void setString(String string) {
    this.string = string;
  }

  public TezosScript prim(String prim) {
    this.prim = prim;
    return this;
  }

   /**
   * Get prim
   * @return prim
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getPrim() {
    return prim;
  }

  public void setPrim(String prim) {
    this.prim = prim;
  }

  public TezosScript args(List<TezosScript> args) {
    this.args = args;
    return this;
  }

  public TezosScript addArgsItem(TezosScript argsItem) {
    this.args.add(argsItem);
    return this;
  }

   /**
   * Get args
   * @return args
  **/
  @ApiModelProperty(example = "null", value = "")
  public List<TezosScript> getArgs() {
    return args;
  }

  public void setArgs(List<TezosScript> args) {
    this.args = args;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TezosScript tezosScript = (TezosScript) o;
    return Objects.equals(this._int, tezosScript._int) &&
        Objects.equals(this.string, tezosScript.string) &&
        Objects.equals(this.prim, tezosScript.prim) &&
        Objects.equals(this.args, tezosScript.args);
  }

  @Override
  public int hashCode() {
    return Objects.hash(_int, string, prim, args);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TezosScript {\n");
    
    sb.append("    _int: ").append(toIndentedString(_int)).append("\n");
    sb.append("    string: ").append(toIndentedString(string)).append("\n");
    sb.append("    prim: ").append(toIndentedString(prim)).append("\n");
    sb.append("    args: ").append(toIndentedString(args)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


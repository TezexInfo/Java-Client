/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * NetworkInfo
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-06T13:45:21.847+01:00")
public class NetworkInfo   {
  @SerializedName("max_level")
  private Integer maxLevel = null;

  @SerializedName("blocktime")
  private String blocktime = null;

  @SerializedName("transactions_24h")
  private Integer transactions24h = null;

  @SerializedName("oeprations_24h")
  private String oeprations24h = null;

  public NetworkInfo maxLevel(Integer maxLevel) {
    this.maxLevel = maxLevel;
    return this;
  }

   /**
   * Get maxLevel
   * @return maxLevel
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getMaxLevel() {
    return maxLevel;
  }

  public void setMaxLevel(Integer maxLevel) {
    this.maxLevel = maxLevel;
  }

  public NetworkInfo blocktime(String blocktime) {
    this.blocktime = blocktime;
    return this;
  }

   /**
   * Get blocktime
   * @return blocktime
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getBlocktime() {
    return blocktime;
  }

  public void setBlocktime(String blocktime) {
    this.blocktime = blocktime;
  }

  public NetworkInfo transactions24h(Integer transactions24h) {
    this.transactions24h = transactions24h;
    return this;
  }

   /**
   * Get transactions24h
   * @return transactions24h
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getTransactions24h() {
    return transactions24h;
  }

  public void setTransactions24h(Integer transactions24h) {
    this.transactions24h = transactions24h;
  }

  public NetworkInfo oeprations24h(String oeprations24h) {
    this.oeprations24h = oeprations24h;
    return this;
  }

   /**
   * Get oeprations24h
   * @return oeprations24h
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getOeprations24h() {
    return oeprations24h;
  }

  public void setOeprations24h(String oeprations24h) {
    this.oeprations24h = oeprations24h;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NetworkInfo networkInfo = (NetworkInfo) o;
    return Objects.equals(this.maxLevel, networkInfo.maxLevel) &&
        Objects.equals(this.blocktime, networkInfo.blocktime) &&
        Objects.equals(this.transactions24h, networkInfo.transactions24h) &&
        Objects.equals(this.oeprations24h, networkInfo.oeprations24h);
  }

  @Override
  public int hashCode() {
    return Objects.hash(maxLevel, blocktime, transactions24h, oeprations24h);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NetworkInfo {\n");
    
    sb.append("    maxLevel: ").append(toIndentedString(maxLevel)).append("\n");
    sb.append("    blocktime: ").append(toIndentedString(blocktime)).append("\n");
    sb.append("    transactions24h: ").append(toIndentedString(transactions24h)).append("\n");
    sb.append("    oeprations24h: ").append(toIndentedString(oeprations24h)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

